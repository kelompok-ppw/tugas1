from django.shortcuts import render
from dashboard.views import harold_image_url, lingedin_logo_url

name = 'Hide The Pain Harold'
about =  'If you wanna learn about hiding your pain, kindly check out my \
          LingedIn profile. I am Hide The Pain Harold.'
birth_date = '12 Dec'
gender = 'Male'
expertise = ['Working', 'Coding', 'Debugging', 'Hide My Pain']
email = 'haroldhidingyourpain@hhp.com'
author = 'Fadhlan Zakiri'
# Create your views here.

def index(request):
    response= {'name': name, 'birth_date': birth_date, 'gender': gender, 'expertise':
               expertise, 'about': about, 'lingedin_logo_url' : lingedin_logo_url,
               'author' : author, 'harold_image_url' : harold_image_url, 'email': email}
    response['app_title'] = 'Biodata'
    html = 'biodata/biodata.html'
    return render(request, html, response)
