from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, name, about, birth_date, gender, expertise, email
from unittest import skip

# Create your tests here.

class BiodataUnitTest(TestCase):

    def test_biodata_url_is_exist(self):
        response = Client().get('/biodata/')
        self.assertEqual(response.status_code,200)

