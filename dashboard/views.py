from django.core.urlresolvers import resolve
from django.shortcuts import render
from add_friend.models import Friend
from update_status.models import Status

# Create your views here.
response = {'author':'Aldi Hilman Ramadhani',
            'app_title':'Statistics',
            'harold_name':'Hide the Pain Harold',}
harold_image_url = 'http://i0.kym-cdn.com/entries/icons/facebook/000/016/546/hidethepainharold.jpg'
lingedin_logo_url = 'https://preview.ibb.co/d1KJow/green.png'
def index(request):
    response['harold_image_url'] = harold_image_url
    response['lingedin_logo_url'] = lingedin_logo_url
    response['friends_count'] = Friend.objects.count()
    response['feeds_count'] = Status.objects.count()
    response['latest_status'] = Status.objects.last()
    html = 'dashboard/dashboard.html'
    return render(request, html, response)
