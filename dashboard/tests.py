from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone

from .views import index
from add_friend.models import Friend
from update_status.models import Status

# Create your tests here.
class DashboardUnitTest(TestCase):

    def test_dashboard_url_is_exist(self):
        response = Client().get('/dashboard/')
        self.assertEqual(response.status_code, 200)

    def test_dashboard_using_index_func(self):
        found = resolve('/dashboard/')
        self.assertEqual(found.func, index)

    def test_showing_navbar_and_footer(self):
        response = Client().get('/dashboard/')
        self.assertTemplateUsed(response, 'dashboard/partials/footer.html')
        self.assertTemplateUsed(response, 'dashboard/partials/header.html')

    def test_dashboard_showing_statistics(self):
        # Creating a new activity
        new_friend = Friend.objects.create(
            name='Aldi Hilman Ramadhani',
            url='http://ppw-aldihilmanr.herokuapp.com',
            created_date=timezone.now(),
        )
        new_status = Status.objects.create(
            status='Suffering from This Life',
            date=timezone.now(),
        )

        # Retrieving all available activity
        counting_all_friend = Friend.objects.all().count()
        counting_all_status = Status.objects.all().count()
        self.assertEqual(counting_all_friend, 1)
        self.assertEqual(counting_all_status, 1)

        response= Client().get('/dashboard/')
        html_response = response.content.decode('utf8')
        self.assertIn('1 People',html_response)
        self.assertIn('1 Post',html_response)

    def test_dashboard_latest_status(self):
        new_status = Status.objects.create(
            status='We Fall In Love with People We Can Not Have',
            date=timezone.now(),
        )
        response= Client().get('/dashboard/')
        html_response = response.content.decode('utf8')
        self.assertIn('We Fall In Love with People We Can Not Have',html_response)
