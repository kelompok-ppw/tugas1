from django.shortcuts import render, redirect
from .models import Friend
from dashboard.views import lingedin_logo_url
import json

# Create your views here.
friend_list = {}
def index(request):
    friend_list = Friend.objects.all().values()
    html = "add_friend/add_friend.html"
    return render(request, html, {'friend_list' : convert_queryset_into_json(friend_list), 'author' : "Zaki Raihan",
        'app_title' : "Friend", 'lingedin_logo_url' : lingedin_logo_url, 'friend' : Friend.objects.all()})

def adding_friend(request):
    if request.method == 'POST':
        Friend.objects.create(name=request.POST['name'], url=request.POST['url'])
        return redirect('/add-friend/')

def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val
