from django.test import TestCase, Client
from django.urls import resolve, reverse
from .views import index
from .models import Friend
from django.utils import timezone


class add_fried_test(TestCase):
	def test_add_friend_url_is_exist(self):
		response = Client().get('/add-friend/')
		self.assertEqual(response.status_code,200)

	def test_add_friend_using_to_do_list_template(self):
		response = Client().get('/add-friend/')
		html = 'add_friend/add_friend.html'
		self.assertTemplateUsed(response, html)

	def test_add_friend_using_index_func(self):
		found = resolve('/add-friend/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_activity(self):
		#Creating a new activity
		new_activity = Friend.objects.create(name='Zakir', url='https://zakiwebpage.herokuapp.com/')

		#Retrieving all available activity
		counting_all_available_activity = Friend.objects.all().count()
		self.assertEqual(counting_all_available_activity,1)

	def test_can_save_a_friend_POST_request(self):
		response = self.client.post('/add-friend/adding_friend/', data={'name':'Zakir', 'url' : 'https://zakiwebpage.herokuapp.com/' ,'created_date': '2017-10-12T14:14'})
		counting_all_available_activity = Friend.objects.all().count()
		self.assertEqual(counting_all_available_activity, 1)

		self.assertEqual(response.status_code, 302)
		self.assertEqual(response['location'], '/add-friend/')

		new_response = self.client.get('/add-friend/')
		html_response = new_response.content.decode('utf8')
		self.assertIn('Zakir', html_response)

	def test_to_string_from_Friend_models(self):
		new_activity = Friend.objects.create(name='Zakir', url='https://zakiwebpage.herokuapp.com/')
		self.assertEqual(str(new_activity), "Zakir memiliki url https://zakiwebpage.herokuapp.com/")
