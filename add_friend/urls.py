from django.conf.urls import url
from .views import index, adding_friend

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^adding_friend/$', adding_friend, name='adding_friend' ),
]