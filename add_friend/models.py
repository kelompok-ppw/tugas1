from django.db import models
from django.utils import timezone

# Create your models here.
class Friend(models.Model):
	name = models.CharField(max_length=50)
	url = models.URLField(max_length=200)
	created_date = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.name + " memiliki url " + self.url