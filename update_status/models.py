from django.db import models

# Create your models here.
class Status(models.Model):
	date = models.DateTimeField(auto_now_add=True)
	status = models.CharField(max_length=2000)
