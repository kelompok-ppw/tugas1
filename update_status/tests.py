from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .views import add_status
from .models import Status
from django.utils import timezone

# Create your tests here.

class Lab3Test(TestCase):
	def test_update_status_url_is_exist(self):
		response = Client().get('/update-status/')
		self.assertEqual(response.status_code,200)

	def test_update_status_using_to_do_list_template(self):
		response = Client().get('/update-status/')
		self.assertTemplateUsed(response, 'base.html')

	def test_update_status_using_index_func(self):
		found = resolve('/update-status/')
		self.assertEqual(found.func, index)

	#def test_model_can_create_new_comment(self):
	#	new_comment = Comment.objects.create(date = timezone.now(), status = "Oke Gw setuju sama status lu")

	#	counting_all_available_comment = Comment.objects.all().count()
	#	self.assertEqual(counting_all_available_comment,1)

	def test_model_can_create_new_status(self):
		#Creating a new activity
		new_status =Status.objects.create(date=timezone.now(),status='Gw mencoba membuat status')
		#Retrieving all available activity
		counting_all_available_status= Status.objects.all().count()
		self.assertEqual(counting_all_available_status,1)

	def test_can_save_a_POST_request(self):
		response = self.client.post('/update-status/add-status/', data={'date' : timezone.now(), 'status' : 'Maen Dota Kayaknya Enak dan seru'})
		counting_all_available_status = Status.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)
		self.assertEqual(response.status_code, 302)
		self.assertEqual(response['location'], '/update-status/')

		new_response = self.client.get('/update-status/')
		html_response = new_response.content.decode('utf8')
		self.assertIn('Maen Dota Kayaknya Enak dan seru', html_response)

