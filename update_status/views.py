from django.shortcuts import render, redirect
from .models import Status
from datetime import datetime
from dashboard.views import lingedin_logo_url
import json

# Create your views here.
status_dict = {}
response = {'lingedin_logo_url' : lingedin_logo_url, 'author' : 'Muhammad Imam Santosa'}
def convert_queryset_into_json(queryset):
    ret_val = []
    for data in queryset:
        ret_val.append(data)
    return ret_val

def index(request):
    status_dict = Status.objects.all().values()
    response['status_dict'] = convert_queryset_into_json(status_dict)
    response['app_title'] = 'Status'
    return render(request, 'base.html', response)


def add_status(request):
	if request.method == 'POST':
		date = datetime
		Status.objects.create(date=date,status=request.POST['status'])
		return redirect('/update-status/')


